-- MariaDB dump 10.19  Distrib 10.5.23-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: pika-db.zih.tu-dresden.de    Database: pika
-- ------------------------------------------------------
-- Server version	10.6.18-MariaDB-deb11

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `footprint_analysis`
--

DROP TABLE IF EXISTS `footprint_analysis`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `footprint_analysis` (
  `uid` bigint(20) NOT NULL,
  `core_idle_ratio` float DEFAULT 0,
  `core_load_imbalance` float DEFAULT 0,
  `core_duration_idle` bigint(20) DEFAULT 0,
  `io_blocking` int(11) DEFAULT 0,
  `mem_leak` float DEFAULT 0,
  `gpu_idle_ratio` float DEFAULT 0,
  `gpu_load_imbalance` float DEFAULT 0,
  `gpu_duration_idle` bigint(20) DEFAULT 0,
  `synchronous_offloading` int(11) DEFAULT 0,
  `io_blocking_detail` varchar(1024) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `io_congestion` int(11) DEFAULT NULL,
  `severity` int(11) DEFAULT NULL,
  PRIMARY KEY (`uid`),
  CONSTRAINT `footprint_analysis_ibfk_1` FOREIGN KEY (`uid`) REFERENCES `job_data` (`uid`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `footprint_base`
--

DROP TABLE IF EXISTS `footprint_base`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `footprint_base` (
  `uid` bigint(20) NOT NULL,
  `ipc_mean_per_core` double DEFAULT NULL,
  `cpu_used_mean_per_core` double DEFAULT NULL,
  `flops_any_mean_per_core` double DEFAULT NULL,
  `mem_bw_mean_per_socket` double DEFAULT NULL,
  `cpu_power_mean_per_socket` double DEFAULT NULL,
  `ib_bw_mean_per_node` double DEFAULT NULL,
  `host_mem_used_max_per_node` double DEFAULT NULL,
  PRIMARY KEY (`uid`),
  CONSTRAINT `footprint_base_ibfk_1` FOREIGN KEY (`uid`) REFERENCES `job_data` (`uid`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `footprint_fileio`
--

DROP TABLE IF EXISTS `footprint_fileio`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `footprint_fileio` (
  `uid` bigint(20) NOT NULL,
  `read_bytes` bigint(20) DEFAULT NULL,
  `write_bytes` bigint(20) DEFAULT NULL,
  `fsname` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`uid`),
  CONSTRAINT `footprint_fileio_ibfk_1` FOREIGN KEY (`uid`) REFERENCES `job_data` (`uid`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `footprint_gpu`
--

DROP TABLE IF EXISTS `footprint_gpu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `footprint_gpu` (
  `uid` bigint(20) NOT NULL,
  `used_mean_per_gpu` double DEFAULT NULL,
  `mem_used_max_per_gpu` double DEFAULT NULL,
  `power_mean_per_gpu` double DEFAULT NULL,
  PRIMARY KEY (`uid`),
  CONSTRAINT `footprint_gpu_ibfk_1` FOREIGN KEY (`uid`) REFERENCES `job_data` (`uid`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `footprint_validation`
--

DROP TABLE IF EXISTS `footprint_validation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `footprint_validation` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `uid` bigint(20) NOT NULL,
  `benchmark_name` varchar(255) DEFAULT NULL,
  `metric_name` varchar(255) DEFAULT NULL,
  `metric_value` double DEFAULT NULL,
  `execution_time` double DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `uid` (`uid`),
  CONSTRAINT `footprint_validation_ibfk_1` FOREIGN KEY (`uid`) REFERENCES `job_data` (`uid`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `job_data`
--

DROP TABLE IF EXISTS `job_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `job_data` (
  `uid` bigint(20) NOT NULL AUTO_INCREMENT,
  `job_id` bigint(20) NOT NULL,
  `user_name` varchar(32) NOT NULL,
  `project_name` varchar(64) DEFAULT NULL,
  `job_state` varchar(128) NOT NULL,
  `node_count` int(11) NOT NULL,
  `node_list` longtext NOT NULL,
  `core_list` longtext NOT NULL,
  `core_count` int(11) DEFAULT NULL,
  `submit_time` bigint(20) DEFAULT NULL,
  `start_time` bigint(20) NOT NULL DEFAULT 0,
  `end_time` bigint(20) DEFAULT NULL,
  `job_name` varchar(256) NOT NULL DEFAULT 'none',
  `time_limit` int(11) NOT NULL,
  `partition_name` varchar(64) NOT NULL,
  `exclusive` tinyint(4) NOT NULL,
  `property_id` tinyint(4) DEFAULT 0,
  `array_id` bigint(20) DEFAULT NULL,
  `tags` bigint(20) DEFAULT NULL,
  `footprint_created` tinyint(4) DEFAULT NULL,
  `gpu_count` int(11) DEFAULT NULL,
  `gpu_list` longtext DEFAULT NULL,
  `smt_mode` tinyint(4) DEFAULT NULL,
  `job_script` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mem_per_cpu` bigint(20) DEFAULT NULL,
  `mem_per_node` bigint(20) DEFAULT NULL,
  `reservation_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`uid`),
  UNIQUE KEY `UJID_INDEX` (`job_id`,`start_time`,`partition_name`) USING BTREE,
  KEY `ARRAY_INDEX` (`array_id`) USING BTREE,
  KEY `SMT_INDEX` (`smt_mode`) USING BTREE,
  KEY `STATUS_INDEX` (`start_time`,`job_state`) USING BTREE,
  KEY `PROJECT_INDEX` (`start_time`,`job_state`,`project_name`) USING BTREE,
  KEY `DURATION_INDEX` (`start_time`,`end_time`) USING BTREE,
  KEY `PENDING_INDEX` (`start_time`,`submit_time`) USING BTREE,
  KEY `USER_INDEX` (`start_time`,`job_state`,`user_name`) USING BTREE,
  KEY `TAG_INDEX` (`start_time`,`tags`) USING BTREE,
  KEY `JOB_INDEX` (`start_time`,`job_state`,`job_name`) USING BTREE,
  KEY `START_INDEX` (`start_time`) USING BTREE,
  KEY `FOOTPRINT_INDEX` (`start_time`,`footprint_created`) USING BTREE,
  KEY `U_USER_INDEX` (`user_name`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `job_queue`
--

DROP TABLE IF EXISTS `job_queue`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `job_queue` (
  `sample_time` bigint(20) NOT NULL,
  `partition_name` varchar(64) NOT NULL,
  `job_id` bigint(20) DEFAULT NULL,
  `array_job_id` bigint(20) DEFAULT NULL,
  `job_name` varchar(256) DEFAULT NULL,
  `user_name` varchar(32) DEFAULT NULL,
  `project_name` varchar(64) DEFAULT NULL,
  `job_state` varchar(32) DEFAULT NULL,
  `node_count` int(11) DEFAULT NULL,
  `scheduled_nodes` longtext DEFAULT NULL,
  `excluded_nodes` longtext DEFAULT NULL,
  `required_nodes` longtext DEFAULT NULL,
  `exclusive` tinyint(4) DEFAULT NULL,
  `cpus` int(11) DEFAULT NULL,
  `requested_cpus` int(11) DEFAULT NULL,
  `gpus` int(11) DEFAULT NULL,
  `requested_gpus` int(11) DEFAULT NULL,
  `memory` bigint(20) DEFAULT NULL,
  `mem_per_cpu_flag` tinyint(4) DEFAULT NULL,
  `time_limit` int(11) DEFAULT NULL,
  `submit_time` bigint(20) DEFAULT NULL,
  `estimate_start_time` bigint(20) DEFAULT NULL,
  `priority` int(11) DEFAULT NULL,
  `state_reason` varchar(128) DEFAULT NULL,
  `reservation_id` int(11) DEFAULT NULL,
  `queue_position` int(11) DEFAULT NULL,
  `dependency` longtext DEFAULT NULL,
  KEY `idx_job_sample_time_partition_name` (`sample_time`,`partition_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `node_info`
--

DROP TABLE IF EXISTS `node_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `node_info` (
  `sample_time` bigint(20) NOT NULL,
  `partition_name` varchar(64) NOT NULL,
  `node_name` varchar(16) DEFAULT NULL,
  `node_state` varchar(32) DEFAULT NULL,
  `alloc_cpus` int(11) DEFAULT NULL,
  `idle_cpus` int(11) DEFAULT NULL,
  `cpu_load` int(11) DEFAULT NULL,
  `alloc_gpus` int(11) DEFAULT NULL,
  `gres_used` varchar(128) DEFAULT NULL,
  `free_mem` bigint(20) DEFAULT NULL,
  `tmp_disk` int(11) DEFAULT NULL,
  `reason` longtext DEFAULT NULL,
  `alloc_mem` bigint(20) DEFAULT NULL,
  `reservation_id` int(11) DEFAULT NULL,
  KEY `idx_node_sample_time_partition_name` (`sample_time`,`partition_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `property`
--

DROP TABLE IF EXISTS `property`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `property` (
  `id` tinyint(4) NOT NULL,
  `property_name` varchar(128) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `reservation`
--

DROP TABLE IF EXISTS `reservation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reservation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `reservation_name` varchar(64) NOT NULL,
  `start_time` bigint(20) DEFAULT NULL,
  `end_time` bigint(20) DEFAULT NULL,
  `node_count` int(11) DEFAULT NULL,
  `node_list` longtext DEFAULT NULL,
  `partition_name` varchar(64) DEFAULT NULL,
  `accounts` varchar(1024) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2024-07-29 17:16:52
