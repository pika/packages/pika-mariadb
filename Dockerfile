FROM mariadb:latest

COPY ./pika-my.cnf /etc/mysql/conf.d
RUN mkdir /pika-setup
COPY ./setup /pika-setup
WORKDIR /pika-setup