#! /bin/bash

docker network ls | grep pika-net > /dev/null || docker network create --driver bridge pika-net
docker compose up -d --build
if [ $? -ne 0 ]; then
  echo "Error: Cannot install containers for pika-mariadb and pika-adminer."
  exit 1
fi

echo "Starting MariaDB..."

if [ -d "data/pika-mariadb/pika" ]; then
    echo "PIKA database already exists."
else
    sleep 10
    echo ""
    echo "Setup passwords for PIKA database:"
    echo ""
    docker exec -it pika-mariadb bash /pika-setup/setup_mariadb.sh
fi
